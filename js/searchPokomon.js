    var result = [];
    //Below array holds all filtered data
    var filteredResults = [];

    $(document).ready(function() {
        $.ajax({
            url: 'https://pokeapi.co/api/v2/pokemon/?limit=151',
            contentType: 'application/json',
            success: function(data) {
                result = data.results;
                $.each(result, function(index, value) {
                    var obj = {};
                    obj['name'] = value.name;
                    var num = (index < 10) ? "00" + (index + 1) : "0" + (index + 1);
                    obj['number'] = num;
                    obj['imagePath'] = "images/" + (index + 1) + ".png";
                    result[index] = obj;
                });
                printResults();
            },
            error: function() {
                $('#displyResults').html("<p> Unable to get pokomon data ");
                $('.sort-wrapper').hide();
            }
        });


        $("#btnSubmit").click(searchPokemon);

        $("#query").on('keyup', searchPokemon);

        function searchPokemon() {
            filteredResults = result;
            var searchString = $("#query").val();
            if (searchString != 'null') {
                filteredResults = $(result).filter(function(idx) {
                    return result[idx].name.toLowerCase().indexOf(searchString.toLowerCase()) !== -1;
                });
                if (filteredResults.length === 0) {
                    $('#displyResults').html("<p> we unable to find any pokoman with name {" + searchString + "}");
                    $('#pagination').html("");
                } else {
                    printResults();
                }
            } else {
                alert("Please enter pokomon name");
            }
        }
        $("#sortByNames").click(function() {
            filteredResults = filteredData();
            filteredResults.sort(function(a, b) {
                var a1 = a.name,
                    b1 = b.name;
                if (a1 == b1) return 0;
                return a1 > b1 ? 1 : -1;
            });
            printResults();
        });
        $("#sortByNumbes").click(function() {
            filteredResults = filteredData();
            filteredResults.sort(function(a, b) {
                var a1 = a.number,
                    b1 = b.number;
                if (a1 == b1) return 0;
                return a1 > b1 ? 1 : -1;
            });
            printResults();
        });

    });

    function printResults() {
        $('#pagination').html(getPagination());
        $('#displyResults').html(getResults(1));
        scrollToMainElem();
    }

    function filteredData() {
        if (filteredResults.length > 0) {
            data = filteredResults;
        } else {
            data = result;
        }
        return data;
    }

    function getNumOfPages() {
        var data = filteredData();
        var size = data.length;
        if (size > 0) {
            var pages = Math.ceil(size / 20);
            if (pages != 1) {
                return pages;
            }
        } else {
            return 0
        }
    }

    function getPagination() {
        var paginationHTML = ''
        var size = getNumOfPages();
        for (i = 1; i <= size; i++) {
            paginationHTML += '<a class="pagination-item num-' + i + '" href="#" onClick="dispalyCurrentPageResults(' + i + ')" >' + i + '</a>';
        }
        return paginationHTML;
    }

    function dispalyCurrentPageResults(pagenum) {
        $('#displyResults').html(getResults(pagenum));
        scrollToMainElem();
    }

    function getResults(pagenum) {
        var activeNum = '.num-' + pagenum;
        $('.pagination-item').css('color', 'black');
        $(activeNum).css('color', 'red');
        var start = (pagenum - 1) * 20;
        var end = pagenum * 20;
        data = filteredData();
        if (end > data.length) {
            end = data.length;
        }
        var displyResultsHtml = '<div class="row grid-x"> ';
        for (var i = start + 1; i <= end; i++) {
            displyResultsHtml += "<div class='single-wrapper small-12 medium-6 large-3 columns'>";
            displyResultsHtml += "<img src=" + data[i - 1].imagePath + "></img>";
            displyResultsHtml += '<span>' + data[i - 1].name + '</span>';
            displyResultsHtml += '<span>#' + data[i - 1].number + '</span>';
            displyResultsHtml += "</div>"
        }
        displyResultsHtml += '</div>'
        return displyResultsHtml;
    }

    function scrollToMainElem() {
        $('html, body').animate({
            scrollTop: $(".main-wrapper").offset().top
        }, 1000);
    }