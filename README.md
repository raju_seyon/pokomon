


Prerequisites
============
1) Node Version v6.10.1
2) NPM Version 3.10.10

Steps
=========
1) Extract zip to a folder
2) Open foler in command/Terminal
3) Run "node server"
4) Open http://localhost:3000/searchPokomon.html in chrome.


Tested in Google Chrome
Version 63.0.3239.84 (Official Build) (64-bit)
