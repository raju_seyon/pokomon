const express = require("express");
const app = express();

app.use(express.static(__dirname));
/*app.get('/searchPokomon', (req, res) => {
  res.sendFile(`${__dirname}/apps/searchPokomon.html`);
});*/

app.listen(3000, () => {
  console.log("Started on http://localhost:3000");
});